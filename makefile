SRCS := $(wildcard src/*.pdc)
HTML := $(SRCS:src/%.pdc=%.html)
SCON := $(SRCS:src/%.pdc=%_sc.html)
NOTE := $(SRCS:src/%.pdc=%_notes.pdf)

.PHONY: all clean reset

simple: $(HTML) index.pdc
	cp -R src/img reveal.js out
	pandoc --standalone \
               --from markdown \
               --to html \
	       --output out/index.html \
	       index.pdc

%.html: src/%.pdc
	pandoc --standalone \
               --from markdown \
               --to revealjs \
               --output out/$@ $<


sc: $(SCON) $(SRCS)

%_sc.html: src/%.pdc
	cp -R src/img .
	pandoc --standalone \
               --from markdown \
               --to revealjs \
               --self-contained \
               --output out/$@ $<
	-rm -rf ./img


notes: $(NOTE) $(SRCS)

%_notes.pdf: src/%.pdc
	cp -R src/img .
	pandoc --standalone \
               --from markdown \
               --to pdf \
               --output out/$@ $<
	-rm -rf ./img


all: simple sc notes

clean:
	-rm -rf out/*

reset: clean all
